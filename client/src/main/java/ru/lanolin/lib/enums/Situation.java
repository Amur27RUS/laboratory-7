package ru.lanolin.lib.enums;

/**
 * Обстановка на мероприятии
 */
public enum Situation {
    RAIN, //Дождь
    NONE, //Обычная погода
    SUN //Солнечная
}
