package ru.lanolin.lib.exception;

public class NullOutfitIntoHumanException extends NullPointerException {

    private String message;

    public NullOutfitIntoHumanException() {
        message = "Вы че? Нельзя так.";
    }

    public NullOutfitIntoHumanException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}