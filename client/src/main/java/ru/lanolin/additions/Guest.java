package ru.lanolin.additions;

import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.human.Human;

import java.time.LocalDate;

public class Guest extends Human {

    private static final long serialVersionUID = 4673673961563758015L;

    public Guest(String name, Feel feel, int quality, String date) {
        super(name, feel, quality, LocalDate.parse(date));
    }

    public Guest(String name, Feel feel) {
        super(name, feel);
    }

    public Guest() {  }

    @Override
    public void rateOutfit(Human other) {
        System.out.println(">" + this.getName() + " оцениват наряд Гостя " + other.getName() + " на " + other.getQuality());
    }

    @Override
    public void great(Human other) {
        System.out.println(">" + this.getName() + " приветвует " + other.getName());
    }

    @Override
    public void bowOut(Human other) {
        System.out.println(">" + this.getName() + " поклонился " + other.getName());
    }
}