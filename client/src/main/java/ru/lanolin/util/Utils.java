package ru.lanolin.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Utils {

//    /**
//     * Скриптовый движок, используется для преобразований {@code JSON} строк в {@link Map}
//     */
//    private static final ScriptEngine engine = new ScriptEngineManager().getEngineByName("javascript");

//    /**
//     * Метод, который преобразует {@code JSON} строку в {@link Map}
//     *
//     * @param element {@code JSON} строку
//     * @return {@link Map}, в котором содержатся ключ-значения из JSON
//     */
//    public static Map<String, Object> parseJSON(String element) {
//        Map<String, Object> result = null;
//        try {
//            result = (Map<String, Object>) engine.eval("Java.asJSONCompatible(" + element + ")");
//        } catch (Exception e) {
//            System.err.println("Внимание!!! Введен неверный формат JSON строки. Исправьте или не продолжайте работать.");
//        }
//        return result;
//    }

    public static <E extends Enum<E>> E getRandomElementEnum(Class<E> en) {
        Enum<E>[] all = en.getEnumConstants();
        int random = (int) (Math.random() * all.length);
        return (E) all[random];
    }

    public static<E> List<E> pickNRandomElements(final List<E> list) {
        ArrayList<E> returnList = new ArrayList<>(list);
        for (int i = 0; i < 5; i++) Collections.shuffle(returnList);
        return returnList.subList(returnList.size() - 2, returnList.size());
    }

    public interface Const{
        long SECOND = (long)1e9;

        double FRAMECAP = 2.0f; //кол-во действий в секунду

        /**
         * Максимальныей размер {@link java.util.Vector}, где хранятся все {@link ru.lanolin.lib.human.Human}
         */
        int SIZE = 256;

        int WorkTimeS = 5; // Время работы мероприятия в секундах
    }

    public static class Time {

        public static long getTime() {
            return System.nanoTime();
        }

        public static boolean isWork(long reactionTime){
            return reactionTime < (1000 / Const.FRAMECAP) && reactionTime >= 0;
        }
    }
}
