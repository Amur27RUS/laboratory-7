package ru.lanolin.util;

import ru.lanolin.additions.Guest;
import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.human.Human;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class XML {

    private static final String StartDocument = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

    private final File xmlFile;

    public XML(File xmlFile) {
        this.xmlFile = xmlFile;
    }

    /**
     * Считывает данные из файлы и переобразует xml в отдельные {@link Human}, которые добаляются в {@link Vector}
     * @return {@link Vector}, в котором хранятся считанные значение
     * @throws IOException ошибка чтения фалйла
     * @throws XMLStreamException ошибка чения XML элемента
     */
    public List<Human> readXMLFile() throws IOException, XMLStreamException {
        List<Human> guests = null;
        Guest guest = null;
        String text = null;

        FileReader br = new FileReader(xmlFile);
        XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(br);

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if ("humans".equals(reader.getLocalName()))
                        guests = new ArrayList<>();
                    if ("human".equals(reader.getLocalName()))
                        guest = new Guest();
                    break;

                case XMLStreamConstants.CHARACTERS:
                    text = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "human":
                            guests.add(guest);
                            guest = null;
                            break;
                        case "name":
                            guest.setName(text);
                            break;
                        case "quality":
                            guest.setQuality(Integer.parseInt(text));
                            break;
                        case "birthday":
                            guest.setBirthday(text);
                            break;
                        case "fate":
                            guest.setFeel(Feel.valueOf(text));
                            break;
                    }
                    break;
            }
        }

        reader.close();
        br.close();
        return guests;
    }

    /**
     * Метод, который преобразует {@link Vector} в XML формат и записывает в файл
     * @param list {@link Vector}, коллекция, в которой хранятся {@link Human} на запись
     * @throws FileNotFoundException ошибка, если не найден файл
     */
    public void writeDocument(List<Human> list) throws FileNotFoundException{
        PrintWriter pw = new PrintWriter(xmlFile);
        pw.write(StartDocument);
        pw.append("<humans>\n");
        list.forEach(h -> pw.append("\t").append(h.toString()).append("\n"));
        pw.append("</humans>");
        pw.close();
    }
}
